# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-04-15 02:12+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Bullet: '  - '
msgid "[[!traillink Start_Tails|first_steps/start_tails]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[!traillink Accessibility|first_steps/accessibility]]"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  - [[!traillink Welcome_Screen|first_steps/welcome_screen]]\n"
"    - [[!traillink Administration_password|first_steps/welcome_screen/administration_password]]\n"
"    - [[!traillink MAC_address_spoofing|first_steps/welcome_screen/mac_spoofing]]\n"
"    - [[!traillink Tor_bridge_mode|first_steps/welcome_screen/bridge_mode]]\n"
"  - [[!traillink Introduction_to_GNOME_and_the_Tails_desktop|first_steps/introduction_to_gnome_and_the_tails_desktop]]\n"
"  - [[!traillink Encrypted_Persistent_Storage|first_steps/persistence]]\n"
"    - [[!traillink Warnings_about_the_Persistent_Storage|first_steps/persistence/warnings]]\n"
"    - [[!traillink Creating_and_configuring_the_Persistent_Storage|first_steps/persistence/configure]]\n"
"    - [[!traillink Unlocking_and_using_the_Persistent_Storage|first_steps/persistence/use]]\n"
"    - [[!traillink Making_a_backup_of_your_Persistent_Storage|first_steps/persistence/copy]]\n"
"    - [[!traillink Deleting_the_Persistent_Storage|first_steps/persistence/delete]]\n"
"  - [[!traillink Installing_additional_software|first_steps/additional_software]]\n"
"  - [[!traillink Reporting_an_error|first_steps/bug_reporting]]\n"
"  - [[!traillink Shutting_down_Tails|first_steps/shutdown]]\n"
msgstr ""

msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"
